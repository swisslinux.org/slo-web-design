Design du future site de Swisslinux.org
=======================================


Ce projet contient quelques designs pour le future site web de
Swisslinux.org. Le dossier `src` contient les fichiers SVG sources.

Le travail de design est réalisé à partir de l'analyse publiées sur notre wiki:
[Besoins de Swisslinux pour son infrastructure Web](https://swisslinux.org/wiki/fr/association/projets/site_internet/mise_a_niveau/besoins_de_swisslinux_pour_son_infrastructure_web)
